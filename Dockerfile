FROM jetty:9-jre11
USER jetty:jetty
MAINTAINER michael.christensen@alexandra.dk

LABEL Description="Dockerfile for the 4S Organizational microservice" Vendor="Alexandra Institute A/S" Version="1.0"

WORKDIR /opt/s4/organizational-service
ADD https://bitbucket.org/4s/ms-scripts/raw/HEAD/wait-for-it.sh .
USER root
RUN ["chmod", "777", "wait-for-it.sh"]
USER jetty:jetty

ADD ./target/service.war /var/lib/jetty/webapps/root.war

WORKDIR /var/lib/jetty

RUN wget https://downloads.jboss.org/keycloak/3.1.0.Final/adapters/keycloak-oidc/keycloak-jetty94-adapter-dist-3.1.0.Final.tar.gz

RUN tar xvzf keycloak-jetty94-adapter-dist-3.1.0.Final.tar.gz

EXPOSE 8080

# The following does NOT start the Jetty server, but simple tells it to create a start.d configuration folder
# and puts a keycloak configuration in that folder:
RUN java -jar $JETTY_HOME/start.jar --create-startd --add-to-start=keycloak

ENV JAVA_OPTIONS ""
ENTRYPOINT ["/bin/bash", "-c", "java ${JAVA_OPTIONS} -XX:+PrintFlagsFinal -XX:+UseContainerSupport -jar /usr/local/jetty/start.jar"]