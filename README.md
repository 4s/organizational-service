# Organizational Service:

| Summary                          |                                                                                                                     |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Service name (computer friendly) | organizational-bff                                                                                                        |
| Maturity level                   | Concept [ ] Prototype [ ] Demonstration [x] Product [ ]                                                                                                         |
| Used externally                  | No                                                                   |
| Database                         | Yes, MariaDB                                                                                                                  |
| RESTful dependencies             | None   |
| Messaging dependencies           | Kafka                                                                                                               |
| Other dependencies               | In DIAS: configuration: User context service (user context info from session ID)                                                            |
| License                          | Apache 2.0                                                                                                          |

This service handles business functionality for FHIR Organization, Practitioner and PractitionerRole resources.

The organizational-service is a microservice intented for use in a microservice based system as the one described at 
[GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md). The service is a 
_stateful business services_ as described in [GMK Backend Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/HFS.md) 
and handles business functionality for FHIR Organization, Practitioner and PractitionerRole resources. See more about 
the use of these FHIR resources and how they relate to other resources in the system on the pages about the 
[FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 

## Documentation

  - [GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md)
  - FHIR API and messaging API documentation: See [docs](docs//) folder.
  - Use of these FHIR resources and how they relate to other resources in the system on the pages about the [FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 
  - Public classes and methods are documented with JavaDoc 
  - More on [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/).

## Prerequisites
To build the project you need:

  * OpenJDK 11+
  * Maven 3.1.1+
  * Docker 20+

The project depends on git submodule (ms-scripts). To initialize this you need to execute:

```
git submodule update --init
git submodule foreach git pull origin master
``` 

If you are running things using docker-compose (see below) you also need to have a docker network by the name of 
`opentele3net` setup:

`docker network create opentele3net` 

## Building the service
The simplest way to build the service is to use the `buildall.sh` script:

`/scripts/buildall.sh`

This will build the service as a WAR file and add this to a Jetty based docker image.

## Running the service
You can run the service just using ``docker run ...`` or you can run the service using the provided docker-compose files. 
You may of course also run the service in container-orchestration environments like Kubernetes or Docker Swarm.

### Running the service with docker-compose
With docker-compose you can run the service like this:

`docker-compose up`

This will start the service with a Jetty running on port 8080. To run the service with a different port mapping use:

`docker-compose -f docker-compose.yml -f docker-compose.portmapping.yml up`

## Logging
The service uses Logback for logging. At runtime Logback log levels can be configured via environment variables (see
below). 

_Notice:_ The service may output person sensitive information at log level `TRACE`. If log level is set to `DEBUG` or 
above no person sensitive information should be output from the service.

If you are running via docker-compose and you want use the fluentd logging driver (if you for instance have setup 
fluentd, elasticsearch and Kibana) instead of the standard json-file you may start the service like this:

```
docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml up
```

## Monitoring
On http the service exposes a health endpoint on the path `/info`. This will return `200 ok` if the service is basically
in a healthy state, meaning that the web service is up and running inside Jetty and the main Kafka consumer of the service
is ready to read messages.

On http the service exposes Prometheus metrics on the path `/metrics`. 

## Environment variables

The files `service.env` and `deploy.env` define all environment variables used by this microservice. `deploy.env` defines
service URLs and log levels that depend on your particular deployment.

*Notice: In production settings database related environment (username/password etc.) will differ from the one
indicated below.* 

| Environment variable          | Required | Description                                                                                                                     |
|-------------------------------|----------|----------------------------------------------------------------------
| SERVICE_NAME                  | y | Computer friendly name of this service. 
| FHIR_VERSION                  | y | Version of HL7 FHIR supported by this service
| *Log levels:*                 ||
| LOG_LEVEL                     | y | Set the log level for 3rd party libraries
| LOG_LEVEL_DK_S4               | y | Set the log level for this service and the 4S libraries used by this service
| *Messaging related:*          | |
| ENABLE_KAFKA                  | y | If `true Kafka communication is enabled for this service
| KAFKA_KEY_SERIALIZER          | y | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_VALUE_SERIALIZER        | y | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_KEY_DESERIALIZER        | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_VALUE_DESERIALIZER      | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_ENABLE_AUTO_COMMIT      | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_AUTO_COMMIT_INTERVAL_MS | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_SESSION_TIMEOUT_MS      | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_GROUP_ID                | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_ACKS                    | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_RETRIES                 | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| ENABLE_HEALTH                 | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| HEALTH_FILE_PATH              | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| HEALTH_INTERVAL_MS            | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| *Database related:*           | | 
| DATABASE_URL                  | y |  Address of MariaDB database instance used as persistent store for the FHIR resources handled by this service.
| DATABASE_USERNAME             | y |  Username for accessing the database
| DATABASE_PASSWORD             | y |  Password for accessing the database
| MYSQL_ALLOW_EMPTY_PASSWORD    | y |  Only set this to `yes`in test and development environments
| MYSQL_DATABASE                | y |  Name of database used as persistent store for the FHIR resources handled by this service.
| MYSQL_USER                    | y |  Username to be setup for accessing the database
| MYSQL_PASSWORD                | y |  Password to be setup for accessing the database
| *Authentication*              | |
| ENABLE_AUTHENTICATION         | n | Enable getting user context information (from http session header or otherwise)
| ENABLE_DIAS_AUTHENTICATION    | n | Enable DIAS specific authentication functionality
| USER_CONTEXT_SERVICE_URL      | n | URL of user context side care service. Required if ENABLE_DIAS_AUTHENTICATION=true
| *Keycloak related*            | | 
| ENABLE_AUTH                   | n |  If `true` Keycloak authentication is enabled for the service
| KEYCLOAK_REALM                | n |  Keycloak Realm used to manage the users and applications that are granted access.
| KEYCLOAK_CLIENT_NAME          | n |  Keycloak Client name - a namespace dedicated to a client.
| AUTH_SERVER_URL               | n |  Should point to `/auth` on your Keycloak server
| *Resource system variables*   | |
| OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM | y |  The system pertaining to the Organization identifier
| OFFICIAL_PRACTITIONER_IDENTIFIER_SYSTEM | y |  The system pertaining to the Practitiner identifier
| OFFICIAL_PRACTITIONERROLE_IDENTIFIER_SYSTEM | y |  The system pertaining to the PractitinerRole identifier

## Test
To do a basic test of the service you may run

```
mvn clean verify
```

### Smoketest without authentication
Set `ENABLE_AUTHENTICATION=false` and `ENABLE_DIAS_AUTHENTICATION=false` and run the service like this:

`docker-compose -f docker-compose.yml -f docker-compose.portmapping.yml up`

``
curl http://localhost:8091/info/
``

Provided you have setup an instance of Kafka on the the same docker network as the service the expected output of the 
curl command is a status code 200. The output should contain:

```
{"status":"all good"}
```

Smoketest the FHIR server part of the service by running:

`curl http://localhost:8091/baseR4/metadata`

This should outout a FHIR CapabilityStatement beginning starting with lines like the following:

``
{
  "resourceType": "CapabilityStatement",
  ...
``

If you have startet an instance of Kafka on the the same docker network as the service is running on you may
smoketest the messaging part of the service by running the following with kafkacat (You can find kafkacat at 
https://github.com/edenhill/kafkacat.): 

`kafkacat -b kafka:9092 -P -t Create_FHIR_Organization src/test/resources/Create_FHIR_Organization.json`

As a result of this you should a.o. see a message on the log saying `processMessage completed successfully with a DataCreated response topic`.

### Smoketest with authentication
To smoketest the service with [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/) 
authentication turned on run Set `ENABLE_AUTHENTICATION=true` and `ENABLE_DIAS_AUTHENTICATION=true` and set 
`USER_CONTEXT_SERVICE_URL` to point to a sidecar service with a [getsessiondata](https://telemed-test.rm.dk/leverandorportal/services/sidevogne/getsessiondata/)
endpoint. After his you may test the service as described above, just adding a valid session id to the header of your
calls:

`curl -H "SESSION: my-valid-session-id" http://localhost:8091/baseR4/metadata`
