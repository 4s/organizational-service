package dk.s4.microservices.organizationalservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import dk.s4.microservices.microservicecommon.fhir.FhirCUDEventProcessor;
import dk.s4.microservices.organizationalservice.servlet.OrganizationalService;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyEventProcessor extends FhirCUDEventProcessor {
    private final Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);

    private final IFhirResourceDao<Organization> organizationDaoR4;
    private final IFhirResourceDao<Practitioner> practitionerDaoR4;
    private final IFhirResourceDao<PractitionerRole> practitionerRoleDaoR4;

    public MyEventProcessor(FhirContext fhirContext,
                            IFhirResourceDao<Organization> organizationDaoR4,
                            IFhirResourceDao<Practitioner> practitionerDaoR4,
                            IFhirResourceDao<PractitionerRole> practitionerRoleDaoR4) {
        super(fhirContext);
        this.organizationDaoR4 = organizationDaoR4;
        this.practitionerDaoR4 = practitionerDaoR4;
        this.practitionerRoleDaoR4 = practitionerRoleDaoR4;
    }

    @Override
    protected IFhirResourceDao daoForResourceType(String resourceType) {
        if (resourceType.equals("Organization"))
            return organizationDaoR4;
        if (resourceType.equals("Practitioner"))
            return practitionerDaoR4;
        if (resourceType.equals("PractitionerRole"))
            return practitionerRoleDaoR4;

        logger.error("No DAO for resource type - should not happen");
        return null;
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        Identifier identifier = null;
        if (resource instanceof Organization) {
            Organization organization = (Organization) resource;
            identifier = organization.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(System.getenv("OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM")))
                    .findAny()
                    .orElse(null);
        }
        else if (resource instanceof Practitioner) {
            Practitioner practitioner = (Practitioner) resource;
            identifier = practitioner.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(System.getenv("OFFICIAL_PRACTITIONER_IDENTIFIER_SYSTEM")))
                    .findAny()
                    .orElse(null);
        }
        else if (resource instanceof PractitionerRole) {
            PractitionerRole practitionerRole = (PractitionerRole) resource;
            identifier = practitionerRole.getIdentifier().stream()
                    .filter(ident -> ident.getSystem().equals(System.getenv("OFFICIAL_PRACTITIONERROLE_IDENTIFIER_SYSTEM")))
                    .findAny()
                    .orElse(null);
        }
        
        return identifier;
    }
}