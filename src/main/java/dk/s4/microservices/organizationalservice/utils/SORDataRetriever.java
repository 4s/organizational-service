package dk.s4.microservices.organizationalservice.utils;

import dk.s4.microservices.organizationalservice.servlet.OrganizationalService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class SORDataRetriever {

    private static final String OUTPUT_FOLDER = "C:\\sourcCode\\projects\\organizational-service\\src\\main\\resources\\Sor";
    private static final String SOR_URL = "http://filer.nsi.dk/sor/data/sor/sorxml/v_2_0_0/Sor.zip";
    private static final String SOR_TARGET_PATH = "target/classes/Sor";

    private final static Logger logger = LoggerFactory.getLogger(OrganizationalService.class);

    /**
     * Retrieves a zipped SOR folder containing the SOR xml, and extracts it in the output folder
     * @throws IOException
     */

    public void retrieveSorAndExtract() throws IOException {
        File sorZip = new File("Sor.zip");
        sorZip.createNewFile();
        FileUtils.copyURLToFile(
                new URL(SOR_URL),
                sorZip);
        unZipIt(sorZip);

    }

    /**
     * Unzips the folder, and places the contained files at target path.
     * @param zipFile the folder to be unzipped
     */
    private void unZipIt(File zipFile){

        byte[] buffer = new byte[1024];

        try{

            //create output directory is not exists
            File folder = new File(OUTPUT_FOLDER);
            if(!folder.exists()){
                folder.mkdir();
            }
            //get the zip file content
            ZipInputStream zis =
                    new ZipInputStream(new FileInputStream(zipFile));
            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while(ze!=null){

                String fileName = ze.getName();
                File newFile = new File(SOR_TARGET_PATH + File.separator + fileName);

                System.out.println("file unzip : "+ newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

            System.out.println("Done");

        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
}
