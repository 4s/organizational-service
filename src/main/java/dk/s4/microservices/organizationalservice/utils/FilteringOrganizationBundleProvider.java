package dk.s4.microservices.organizationalservice.utils;

import ca.uhn.fhir.rest.api.server.IBundleProvider;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class FilteringOrganizationBundleProvider {

    private final Set<String> blacklist;

    public FilteringOrganizationBundleProvider(String blackListString) {
        blacklist = constructBlackList(blackListString);
    }

    public Bundle filter(IBundleProvider provider) {
        Bundle bundle = new Bundle();

        if (provider.size() == 0) {
            return new Bundle();
        }

        List<IBaseResource> resources = provider.getResources(0, provider.size());
        for (IBaseResource resource : resources) {
            bundle.addEntry(new Bundle.BundleEntryComponent().setResource((Organization) resource));
        }

        return filter(bundle);
    }


    public Bundle filter(Bundle bundle) {
        Bundle filteredBundle = new Bundle();
        for (Bundle.BundleEntryComponent entryComponent : bundle.getEntry()) {
            Organization organization = (Organization) entryComponent.getResource();
            if (shouldIgnore(organization.getIdentifierFirstRep())) continue;

            filteredBundle.addEntry(entryComponent);
        }
        return filteredBundle;
    }

    protected Set<String> constructBlackList(String blackListString) {
        Set<String> returnSet = new HashSet<>();
        StringTokenizer tokenizer = new StringTokenizer(blackListString, ",");
        while (tokenizer.hasMoreTokens()) {
            returnSet.add(tokenizer.nextToken());
        }
        return returnSet;
    }

    private boolean shouldIgnore(Identifier identifier) {
        return blacklist.contains(identifier.getValue());
    }
}
