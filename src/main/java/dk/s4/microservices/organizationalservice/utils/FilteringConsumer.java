package dk.s4.microservices.organizationalservice.utils;

import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import org.hl7.fhir.r4.model.Organization;

import java.util.function.Consumer;

public class FilteringConsumer implements Consumer<Organization> {

    private final IFhirResourceDao<Organization> dao;

    public FilteringConsumer(IFhirResourceDao<Organization> dao){
        this.dao = dao;
    }

    @Override
    public void accept(Organization organization) {
        if(isAccepted(organization)){
            dao.create(organization);
        }
    }

    protected boolean isAccepted(Organization organization) {
        return true;
    }
}
