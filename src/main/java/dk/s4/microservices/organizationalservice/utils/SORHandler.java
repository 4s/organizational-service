package dk.s4.microservices.organizationalservice.utils;

import org.hl7.fhir.r4.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * This class handles loading organizations from the SOR-database XML into the dao of the servlet.
 */

public class SORHandler {

    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(SORHandler.class);
    private static String regionSorString = System.getenv("SOR_ENTITY_TYPE_IDENTIFIER");
    private static Document mySorDataTypesDocument;

    private static final String INSTITUTION_OWNER_ENTITY = "InstitutionOwnerEntity";
    private static final String INSTITUTION_OWNER = "InstitutionOwner";
    private static final String HEALTH_INSTITUTE_ENTITY = "HealthInstitutionEntity";
    private static final String HEALTH_INSTITUTE = "HealthInstitution";
    private static final String ORGANIZATIONAL_UNIT_ENTITY = "OrganizationalUnitEntity";
    private static final String ORGANIZATIONAL_UNIT = "OrganizationalUnit";

    private static final String SOR_IDENTIFIER = "SorIdentifier";
    private static final String ENTITY_NAME = "EntityName";
    private static final String ENTITY_TYPE_IDENTIFIER = "EntityTypeIdentifier";
    private static final String POSTAL_ADDRESS = "PostalAddressInformation";
    private static final String VIRTUAL_ADDRESS = "VirtualAddressInformation";

    private final XMLInputFactory factory = XMLInputFactory.newInstance();
    private final FilteringConsumer consumer;

    /**
     * Constructor. Parses the SorDataTypes.xml file into mySorDataTypesDocument, such that SorTypes can be looked up
     *
     * @param consumer A filtering consumer that can accept Organizations
     * @throws Exception
     */
    public SORHandler(FilteringConsumer consumer) throws Exception {
        this.consumer = consumer;
        ClassLoader classLoader = getClass().getClassLoader();

        File sorDataTypesXml = new File(classLoader.getResource("Sor/SorDataTypes.xml").getFile());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
        mySorDataTypesDocument = documentBuilder.parse(sorDataTypesXml);
        mySorDataTypesDocument.getDocumentElement().normalize();

    }

    /**
     * Parses the Sor.xml file, and calls parseIOE for every InstitutionOwnerEntity-child of the root.
     *
     * @throws XMLStreamException
     */

    public void parse() throws XMLStreamException {

        final InputStream stream = this.getClass().getResourceAsStream("/Sor/Sor.xml");
        final XMLEventReader reader = factory.createXMLEventReader(stream);
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(INSTITUTION_OWNER_ENTITY)) {
                parseIOE(reader);
            }
        }
    }

    /**
     * Parses InstitutionOwnerEntity entries
     *
     * @param reader
     * @throws XMLStreamException
     */

    private void parseIOE(XMLEventReader reader) throws XMLStreamException {
        Organization organization = new Organization();
        boolean isInIOE = false;
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            // If we have reached and end element of type InstitutionOwnerEntity, then we are done parsing the InstitutionOwnerEntity, and we return. That is, if we meet "</InstitutionOwnerEntity>" then we end.
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(INSTITUTION_OWNER_ENTITY)) {
                return;
            }
            // If we meet an InstitutionOwner entry, then we parse by calling parseIO, and save the resulting organization.
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(INSTITUTION_OWNER)) {
                organization = parseIO(reader);
                isInIOE = organization.hasIdentifier();
            }
            if (isInIOE) {
                // If we  meet a HealthInstituteEntity, we call parseHIE, with the organization that resulted from parsing the InstituteOwner entry as parent.
                // As the parser parse the document top down, the InstituteOwner entry will always be parsed before the HealthInstituteEntity
                if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(HEALTH_INSTITUTE_ENTITY)) {
                    parseHIE(reader, organization);
                }
            }
        }
    }

    /**
     * Parses HealthInstituteEntity entries
     *
     * @param reader
     * @param parent the parent organization of the HealthInstituteEntity
     * @throws XMLStreamException
     */

    private void parseHIE(XMLEventReader reader, Organization parent) throws XMLStreamException {
        Organization organization = new Organization();
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(HEALTH_INSTITUTE_ENTITY)) {
                // If we have reached and end element of type HealthInstituteEntity, then we are done parsing the HealthInstituteEntity, and we return. That is, if we meed "</HealthInstituteEntity>" then we end.
                return;
            }
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(HEALTH_INSTITUTE)) {
                // If we meet an HealthInstitute entry, then we parse it by calling parseOrganization, and save the resulting organization.
                organization = parseOrganization(reader, parent, HEALTH_INSTITUTE);
            }
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(ORGANIZATIONAL_UNIT_ENTITY)) {
                // If we  meet a OrganizationalUnitEntity, we call parseOUE, with the organization that resulted from parsing the HealthInstitute entry as parent.
                // As the parser parse the document top down, the HealthInstitute entry will always be parsed before the OrganizationalUnitEntity
                parseOUE(reader, organization);
            }
        }
    }

    /**
     * Parses OrganizationalUnitEntity entries.
     *
     * @param reader
     * @param parent the parent organization of the OrganizationalUnitEntity
     * @throws XMLStreamException
     */

    private void parseOUE(XMLEventReader reader, Organization parent) throws XMLStreamException {
        Organization organization = null;
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(ORGANIZATIONAL_UNIT_ENTITY)) {
                // If we have reached and end element of type OrganizationalUnitEntity, then we are done parsing the OrganizationalUnitEntity, and we return. That is, if we meed "</OrganizationalUnitEntity>" then we end.
                return;
            }
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(ORGANIZATIONAL_UNIT)) {
                // If we meet an OrganizationalUnit entry, then we parse it by calling parseOrganization, and save the resulting organization.
                organization = parseOrganization(reader, parent, ORGANIZATIONAL_UNIT);
            }
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(ORGANIZATIONAL_UNIT_ENTITY)) {
                // If we  meet a OrganizationalUnitEntity, we call parseOUE, with the organization that resulted from parsing the OrganizationalUnit entry as parent.
                // As the parser parse the document top down, the OrganizationalUnit entry will always be parsed before the OrganizationalUnitEntity
                // As OrganizationalUnitEntity entries can contain OrganizationalUnitEntities, this is a recursive call.
                parseOUE(reader, organization);
            }
        }
    }

    /**
     * Parse Organization and create it in Dao
     *
     * @param reader
     * @param parent      parent Organization
     * @param elementType Type of Organization (HealthInstitute or Organizational Unit)
     * @return The created organization
     * @throws XMLStreamException
     */

    private Organization parseOrganization(XMLEventReader reader, Organization parent, String elementType) throws XMLStreamException {
        Organization organization = new Organization();
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            // If we see an end element: "</...>" of type elementType, we finalize the organization, and create it in the dao.
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(elementType)) {

                // If the organization does not have a telecom, it will inherent the telecom of the parent.
                if (!organization.hasTelecom()) {
                    organization.setTelecom(parent.getTelecom());
                }
                // If the organization does not have an address, it will inherent the address of the parent.
                if (!organization.hasAddress()) {
                    organization.setAddress(parent.getAddress());
                }
                // We set the 'partOf' field to be equal to the identifier of the parent.
                // As we do not call this method with elementType = InstitutionOwnerEntity, the parent is never null.
                organization.setPartOf(new Reference().setIdentifier(
                        new Identifier().setValue(parent.getIdentifier().get(0).getValue())
                                .setSystem(parent.getIdentifier().get(0).getSystem()))
                        .setType("Organization"));
                consumer.accept(organization);
                ourLog.debug("Created organization in dao: " + organization.getName());
                return organization;
            }
            // If the element read is a start element, that is it looks like "<elementName>" not "</elementName>", then we switch on the elementName
            if (event.isStartElement()) {
                final StartElement element = event.asStartElement();
                final String elementName = element.getName().getLocalPart();
                switch (elementName) {
                    case SOR_IDENTIFIER: // In this case, the element is on the form "<sor1:SorIdentifier>_identifier_</sor1:SorIdentifier>"
                        String elementIdentifier = reader.getElementText();
                        List<Identifier> identifierList = new ArrayList<>();
                        identifierList.add(new Identifier().setSystem("urn:oid:1.2.208.176.1.1").setValue(elementIdentifier));
                        organization.setIdentifier(identifierList);
                        break;
                    case ENTITY_NAME: // In this case, the element is on the form "<sor1:EntityName>_name_</sor1:EntityName>"
                        organization.setName(reader.getElementText());

                        break;
                    case ENTITY_TYPE_IDENTIFIER: // In this case, the element is on the form "<EntityTypeIdentifier>_code_</EntityTypeIdentifier>"
                        // Each code corresponds to a plaintext Type, which is defined in SorTypes.xml document.
                        // We collect this plaintext type, by calling typeConverter(_code_), and add this to our organization.
                        CodeableConcept SorCode = new CodeableConcept();
                        List<Coding> codingList = new ArrayList<>();
                        Coding coding = new Coding();
                        coding.setSystem("http://sor.sundhedsstyrelsen.dsdn.dk/lookupdata/#SorCodes");
                        String codeString = typeConverter(reader.getElementText());
                        coding.setCode(codeString);
                        coding.setDisplay(codeString);
                        codingList.add(coding);
                        SorCode.setCoding(codingList);
                        organization.addType(SorCode);
                        break;
                    case POSTAL_ADDRESS: // In this case, the element is on the form "<PostalAddressInformation>", which has child entries.
                        // We parse this by calling parsePostal.
                        parsePostal(reader, organization);
                        break;
                    case VIRTUAL_ADDRESS: // In this case, the element is on the form "<sor1:VirtualAddressInformation>", which has child entries.
                        // We parse this by calling parseVirtual.
                        ContactPoint contactPoint = new ContactPoint();
                        contactPoint.setSystem(ContactPoint.ContactPointSystem.PHONE);
                        contactPoint.setUse(ContactPoint.ContactPointUse.WORK);
                        contactPoint.setValue(parseVirtual(reader));
                        organization.addTelecom(contactPoint);
                        break;
                }
            }
        }
        return organization;
    }

    /**
     * Parse InstitutionOwner and create organization in Dao. Tests whether ID of current InstitutionOwner matches regionSorString. If not, no Organization is created.
     *
     * @param reader
     * @return created organization
     * @throws XMLStreamException
     */

    private Organization parseIO(XMLEventReader reader) throws XMLStreamException {
        Organization organization = new Organization();
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            // If we see an end element: "</...>" of type elementType, we create it in the dao.
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(INSTITUTION_OWNER)) {
                consumer.accept(organization);
                ourLog.debug("Created organization in dao: " + organization.getName());
                return organization;
            }
            // If the element read is a start element, that is it looks like "<elementName>" not "</elementName>", then we switch on the elementName
            if (event.isStartElement()) {
                final StartElement element = event.asStartElement();
                final String elementName = element.getName().getLocalPart();
                switch (elementName) {
                    case SOR_IDENTIFIER: // In this case, the element is on the form "<sor1:SorIdentifier>_identifier_</sor1:SorIdentifier>"
                        String elementIdentifier = reader.getElementText();
                        if (elementIdentifier.equals(regionSorString)){
                            List<Identifier> identifierList = new ArrayList<>();
                            identifierList.add(new Identifier().setSystem("urn:oid:1.2.208.176.1.1").setValue(elementIdentifier));
                            organization.setIdentifier(identifierList);
                        } else {
                            return organization;
                        }
                        break;
                    case ENTITY_NAME: // In this case, the element is on the form "<sor1:EntityName>_name_</sor1:EntityName>"
                        organization.setName(reader.getElementText());

                        break;
                    case ENTITY_TYPE_IDENTIFIER:  // In this case, the element is on the form "<EntityTypeIdentifier>_code_</EntityTypeIdentifier>"
                        // Each code corresponds to a plaintext Type, which is defined in SorTypes.xml document.
                        // We collect this plaintext type, by calling typeConverter(_code_), and add this to our organization.
                        CodeableConcept SorCode = new CodeableConcept();
                        List<Coding> codingList = new ArrayList<>();
                        Coding coding = new Coding();
                        coding.setSystem("http://sor.sundhedsstyrelsen.dsdn.dk/lookupdata/#SorCodes");
                        String codeString = typeConverter(reader.getElementText());
                        coding.setCode(codeString);
                        coding.setDisplay(codeString);
                        codingList.add(coding);
                        SorCode.setCoding(codingList);
                        organization.addType(SorCode);
                        break;
                    case POSTAL_ADDRESS: // In this case, the element is on the form "<PostalAddressInformation>", which has child entries.
                        // We parse this by calling parsePostal.
                        parsePostal(reader, organization);
                        break;
                    case VIRTUAL_ADDRESS: // In this case, the element is on the form "<sor1:VirtualAddressInformation>", which has child entries.
                        // We parse this by calling parseVirtual.
                        ContactPoint contactPoint = new ContactPoint();
                        contactPoint.setSystem(ContactPoint.ContactPointSystem.PHONE);
                        contactPoint.setUse(ContactPoint.ContactPointUse.WORK);
                        contactPoint.setValue(parseVirtual(reader));
                        organization.addTelecom(contactPoint);
                        break;
                }
            }
        }
        return organization;
    }

    /**
     * Parse VirtualAddressInformation
     *
     * @param reader
     * @return String of the phonenumber of the given VirtualAddressInformation
     * @throws XMLStreamException
     */

    private String parseVirtual(XMLEventReader reader) throws XMLStreamException {
        String phoneNumberString = "";
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(VIRTUAL_ADDRESS)) {
                return phoneNumberString;
            }
            if (event.isStartElement()) {
                final StartElement element = event.asStartElement();
                final String elementName = element.getName().getLocalPart();
                if (elementName.equals("TelephoneNumberIdentifier")) {
                    phoneNumberString = reader.getElementText();
                }
            }
        }
        return phoneNumberString;
    }

    /**
     * Parse PostalAddressInformation, and adds it to the organization currently being constructed
     *
     * @param reader
     * @param organization the organization currently being constructed
     * @return the organization with the address added
     * @throws XMLStreamException
     */

    private void parsePostal(XMLEventReader reader, Organization organization) throws XMLStreamException {
        Address address = new Address();
        address.setUse(Address.AddressUse.WORK);
        address.setType(Address.AddressType.POSTAL);
        String streetName = "";
        String streetNumber = "";
        List<StringType> line = new ArrayList<>();
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(POSTAL_ADDRESS)) {
                line.add(new StringType(streetName + " " + streetNumber));
                address.setLine(line);
                organization.addAddress(address);
                return;
            }
            if (event.isStartElement()) {
                final StartElement element = event.asStartElement();
                final String elementName = element.getName().getLocalPart();
                switch (elementName) {
                    case "StreetName":
                        streetName = reader.getElementText();
                        break;
                    case "StreetBuildingIdentifier":
                        streetNumber = reader.getElementText();
                        break;
                    case "DistrictName":
                        address.setCity(reader.getElementText());
                        break;
                    case "PostCodeIdentifier":
                        address.setPostalCode(reader.getElementText());
                        break;
                    case "CountryIdentificationCode":
                        address.setCountry(reader.getElementText());
                        break;
                }
            }
        }
    }

    /**
     * Converts the EntityTypeIdentifier to a human-readable Type String, by looking up in the SorDataTypesDocument.
     *
     * @param code
     * @return
     */

    private String typeConverter(String code) {
        String retVal = "";
        NodeList sorDataTypesCollection = mySorDataTypesDocument.getDocumentElement().getChildNodes(); // Parse the SORTypes document as a dom.
        for (int i = 5; i < sorDataTypesCollection.getLength(); i = i + 2) { // search every entry. The first 5 entries in the document doesn't matter. We increment by two, as both start elements and end elements count.
            NodeList childCollection = sorDataTypesCollection.item(i).getChildNodes();
            for (int j = 1; j < childCollection.getLength(); j = j + 2) {
                if (childCollection.item(j).getChildNodes().item(1).getTextContent().equals(code)) { //check if the entry 'EntityTypeIdentifier' match code
                    return childCollection.item(j).getChildNodes().item(3).getTextContent(); //return the entry 'TypeName'
                }
            }
        }
        return retVal;
    }
}
