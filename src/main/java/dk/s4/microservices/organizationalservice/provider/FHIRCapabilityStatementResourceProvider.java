package dk.s4.microservices.organizationalservice.provider;

import ca.uhn.fhir.jpa.rp.r4.CapabilityStatementResourceProvider;
import org.hl7.fhir.r4.model.CapabilityStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FHIRCapabilityStatementResourceProvider extends CapabilityStatementResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIROrganizationResourceProvider.class);

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<CapabilityStatement> getResourceType() {
        return CapabilityStatement.class;
    }



}
