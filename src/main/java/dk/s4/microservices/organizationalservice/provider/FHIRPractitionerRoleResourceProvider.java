package dk.s4.microservices.organizationalservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.Count;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Patch;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.annotation.Sort;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.HasAndListParam;
import ca.uhn.fhir.rest.param.ReferenceAndListParam;
import ca.uhn.fhir.rest.param.StringAndListParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.organizationalservice.servlet.OrganizationalService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents a resource provider for the FHIR Practitioner resource,
 * which supports the operations: read, create, update, and search.
 * 
 */
public class FHIRPractitionerRoleResourceProvider extends BaseJpaResourceProvider<PractitionerRole> {
    private static final Logger logger = LoggerFactory.getLogger(FHIRPractitionerRoleResourceProvider.class);

	private KafkaEventProducer kafkaCommandProducer;


	public FHIRPractitionerRoleResourceProvider(IFhirResourceDao<PractitionerRole> dao, FhirContext fhirContext) {
		super(dao);
		setContext(fhirContext);
		try {
			kafkaCommandProducer = new KafkaEventProducer(System.getenv("SERVICE_NAME"));
		} catch (KafkaInitializationException e) {
			e.printStackTrace();
		}
	}
	/**
	 * The getResourceType method comes from IResourceProvider, and must be
	 * overridden to indicate what type of resource this provider supplies.
	 */
	@Override
	public Class<PractitionerRole> getResourceType() {
		return PractitionerRole.class;
	}

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
            RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public PractitionerRole read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

    /**
     * Generic search API for retrieval of Bundles of PractitonerRoles stored by this service
     *
     * @param theServletRequest Incoming http request
     * @param theServletResponse	Outgoing http response
     * @param theRequestDetails
     * @param theFtContent Search the contents of the resource's data using a fulltext search
     * @param theHas Return resources linked to by the given target
     * @param the_id The ID of the resource
     * @param theActive Whether this practitioner role record is in active use
     * @param theDate The period during which the practitioner is authorized to perform in these role(s)
     * @param theIdentifier A practitioner's Identifier
     * @param theOrganization The identity of the organization the practitioner represents / acts on behalf of
     * @param thePractitioner Practitioner that is able to provide the defined services for the organization
     * @param theService The list of healthcare services that this worker provides for this role's Organization/Location(s)
     * @param theLastUpdated Only return resources which were last updated as specified by the given range
     * @param theSort Sort order
     * @param theCount
     * @return Bundle of PractitionerRole resources
     */

    @Search
    public IBundleProvider search(HttpServletRequest theServletRequest,
            HttpServletResponse theServletResponse,
            RequestDetails theRequestDetails,
            @Description(shortDefinition = "Search the contents of the resource's data using a fulltext search")
            @OptionalParam(name = "_content") StringAndListParam theFtContent,
            @Description(shortDefinition = "Return resources linked to by the given target")
            @OptionalParam(name = "_has") HasAndListParam theHas,
            @Description(shortDefinition = "The ID of the resource")
            @OptionalParam(name = "_id") TokenAndListParam the_id,
            @Description(shortDefinition = "Whether this practitioner role record is in active use")
            @OptionalParam(name = "active") TokenAndListParam theActive,
            @Description(shortDefinition = "The period during which the practitioner is authorized to perform in these role(s)")
            @OptionalParam(name = "date") DateRangeParam theDate,
            @Description(shortDefinition = "A practitioner's Identifier")
            @OptionalParam(name = "identifier") TokenAndListParam theIdentifier,
            @Description(shortDefinition = "The identity of the organization the practitioner represents / acts on behalf of")
            @OptionalParam(name = "organization",targetTypes = {}) ReferenceAndListParam theOrganization,
            @Description(shortDefinition = "Practitioner that is able to provide the defined services for the organization")
            @OptionalParam(name = "practitioner",targetTypes = {}) ReferenceAndListParam thePractitioner,
            @Description(shortDefinition = "The list of healthcare services that this worker provides for this role's Organization/Location(s)")
            @OptionalParam(name = "service",targetTypes = {}) ReferenceAndListParam theService,
            @Description(shortDefinition = "Identifier part of logical reference to organization")
            @OptionalParam(name = "organizationIdentifier") TokenParam theOrganizationIdentifier,
            @Description(shortDefinition = "Identifier part of logical reference to practitioner")
            @OptionalParam(name = "practitionerIdentifier") TokenParam thePractitionerIdentifier,
            @Description(shortDefinition = "Only return resources which were last updated as specified by the given range")
            @OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,
            @Sort SortSpec theSort,
            @Count Integer theCount) {
        this.startRequest(theServletRequest);

        IBundleProvider iBundleProvider;
        try {
            SearchParameterMap paramMap = new SearchParameterMap();
            paramMap.add("_content", theFtContent);
            paramMap.add("_has", theHas);
            paramMap.add("_id", the_id);
            paramMap.add("active", theActive);
            paramMap.add("date", theDate);
            paramMap.add("identifier", theIdentifier);
            paramMap.add("organization", theOrganization);
            paramMap.add("practitioner", thePractitioner);
            paramMap.add("service", theService);
            paramMap.add("organizationIdentifier", theOrganizationIdentifier);
            paramMap.add("practitionerIdentifier", thePractitionerIdentifier);
            paramMap.setLastUpdated(theLastUpdated);
            paramMap.setSort(theSort);
            paramMap.setCount(theCount);
            IBundleProvider retVal = this.getDao().search(paramMap, theRequestDetails, theServletResponse);
            iBundleProvider = retVal;
        } finally {
            this.endRequest(theServletRequest);
        }

        return iBundleProvider;
    }

}
