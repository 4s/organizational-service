package dk.s4.microservices.organizationalservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.Count;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Patch;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.annotation.Sort;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.SearchTotalModeEnum;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.SummaryEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.StringAndListParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.organizationalservice.servlet.OrganizationalService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents a resource provider for the FHIR Practitioner resource,
 * 
 */
public class FHIRPractitionerResourceProvider extends BaseJpaResourceProvider<Practitioner> {
	private static final Logger logger = LoggerFactory.getLogger(FHIRPractitionerResourceProvider.class);

	private IFhirResourceDao<PractitionerRole> practitionerRoleDaoR4;

	public FHIRPractitionerResourceProvider(IFhirResourceDao<PractitionerRole> practitionerRoleDaoR4, IFhirResourceDao<Practitioner> dao, FhirContext fhirContext) {
		super(dao);
		setContext(fhirContext);
		this.practitionerRoleDaoR4 = practitionerRoleDaoR4;
	}

	/**
	 * The getResourceType method comes from IResourceProvider, and must be
	 * overridden to indicate what type of resource this provider supplies.
	 */
	@Override
	public Class<Practitioner> getResourceType() {
		return Practitioner.class;
	}

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
            RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public Practitioner read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

	/**
	 * Search for Practitioners connected to given Organization, through Practitioner Roles stored by this service
	 * @param theRequest
	 * @param theResponse
	 * @param theOrganization
	 * @return
	 */

	@Search(queryName = "getOrganizationPractitioners")
	public Bundle getOrganizationPractitioners(HttpServletRequest theRequest, HttpServletResponse theResponse,
											   RequestDetails theRequestDetails,
											   @Description(shortDefinition = "The identification string of the chosen Organization")
											   @RequiredParam(name = PractitionerRole.SP_ORGANIZATION) TokenParam theOrganization) {
		logger.debug("getOrganizationPractitioners");
		Bundle retVal = new Bundle();

		try {
			this.startRequest(theRequest);


			SearchParameterMap searchParameterMap = new SearchParameterMap();
			searchParameterMap.add("organizationIdentifier", theOrganization);
			searchParameterMap.add("active", new TokenParam().setValue("true"));
			IBundleProvider practitionerRoleBundleProvider = practitionerRoleDaoR4.search(searchParameterMap, theRequestDetails, theResponse);

			if(practitionerRoleBundleProvider.size() == 0){
				return retVal;
			}

			// create new parameter map for searching Practitioners
			SearchParameterMap parameterMap = new SearchParameterMap();
			TokenOrListParam referenceOrListParam = new TokenOrListParam();
			List<IBaseResource> practitionerRoles = practitionerRoleBundleProvider.getResources(0,practitionerRoleBundleProvider.size());
			for (IBaseResource practitionerRole : practitionerRoles){
				if(((PractitionerRole) practitionerRole).getActive()) {
					Reference practitionerTarget = ((PractitionerRole) practitionerRole).getPractitioner();
					referenceOrListParam.addOr(new TokenParam(
							practitionerTarget.getIdentifier().getSystem(),
							practitionerTarget.getIdentifier().getValue()
					));
				}
			}
			parameterMap.add("identifier",referenceOrListParam);
			IBundleProvider practitionerBundleProvider = this.getDao().search(parameterMap,theRequestDetails,theResponse);

			if(practitionerBundleProvider.size() == 0){
				return retVal;
			}

			List<IBaseResource> practitioners = practitionerBundleProvider.getResources(0,practitionerBundleProvider.size());
			for (IBaseResource practitioner : practitioners){
				String classString = practitioner.getClass().getName();
				if (classString.equals("org.hl7.fhir.r4.model.Practitioner")){
					retVal.addEntry().setResource((Practitioner) practitioner);
				}
			}
		} finally {
			this.endRequest(theRequest);
		}
		return retVal;
	}

    /**
	 * Generic search API for retrieval of Bundles of Practitioners stored by this service
	 *
	 * @param theServletRequest Incoming http request
	 * @param theServletResponse Outgoing http request
	 * @param theRequestDetails
	 * @param theFtContent Search the constents of the resource's data using a fulltext search
	 * @param the_id The ID of the resource
	 * @param theFamily A portion of the family name of the practitioner
	 * @param theGiven A portion of the given name of the practitioner
	 * @param theIdentifier	A practitioner's Identifier
	 * @param theName A server defined search that may match any of the string fields in the HumanName, including family, give, prefix, suffix, suffix, and/or text
	 * @param theLastUpdated Only return resources which were last updated as specified by the given range
	 * @param theSort Sort order
	 * @param theCount
	 * @param theSummaryMode
	 * @param theSearchTotalMode
	 * @return
	 */
	@Search()
	public IBundleProvider search(HttpServletRequest theServletRequest, HttpServletResponse theServletResponse,
			RequestDetails theRequestDetails,
			@Description(shortDefinition = "Search the contents of the resource's data using a fulltext search")
			@OptionalParam(name = "_content") StringAndListParam theFtContent,
			@Description(shortDefinition = "The ID of the resource")
			@OptionalParam(name = "_id") TokenAndListParam the_id,
            @Description(shortDefinition = "Whether the practitioner record is active")
            @OptionalParam(name = "active") TokenAndListParam theActive,
			@Description(shortDefinition = "A portion of the family name")
			@OptionalParam(name = "family") StringAndListParam theFamily,
			@Description(shortDefinition = "A portion of the given name")
			@OptionalParam(name = "given") StringAndListParam theGiven,
			@Description(shortDefinition = "A practitioner's Identifier")
			@OptionalParam(name = "identifier") TokenAndListParam theIdentifier, // Token-OR-ListParam
			@Description(shortDefinition = "A server defined search that may match any of the string fields in the HumanName, including family, give, prefix, suffix, suffix, and/or text")
			@OptionalParam(name = "name") StringAndListParam theName,
			@Description(shortDefinition = "Only return resources which were last updated as specified by the given range")
			@OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,
			@Sort SortSpec theSort,
			@Count Integer theCount,
			SummaryEnum theSummaryMode,
			SearchTotalModeEnum theSearchTotalMode) {
		this.startRequest(theServletRequest);

		IBundleProvider bundleProvider;
		try {
			SearchParameterMap paramMap = new SearchParameterMap();
			paramMap.add("_content", theFtContent);
			paramMap.add("_id", the_id);
            paramMap.add("active", theActive);
            paramMap.add("family", theFamily);
			paramMap.add("given", theGiven);
			paramMap.add("identifier", theIdentifier);
			paramMap.add("name", theName);
			paramMap.setLastUpdated(theLastUpdated);
			paramMap.setSort(theSort);
			paramMap.setCount(theCount);
			paramMap.setSummaryMode(theSummaryMode);
			paramMap.setSearchTotalMode(theSearchTotalMode);
			IBundleProvider retVal = this.getDao().search(paramMap, theRequestDetails, theServletResponse);
			bundleProvider = retVal;
		} finally {
			this.endRequest(theServletRequest);
		}

		return bundleProvider;
	}
}
