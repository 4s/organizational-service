package dk.s4.microservices.organizationalservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.Count;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Patch;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.annotation.Sort;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.SearchTotalModeEnum;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.SummaryEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.HasAndListParam;
import ca.uhn.fhir.rest.param.ReferenceAndListParam;
import ca.uhn.fhir.rest.param.StringAndListParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dk.s4.microservices.organizationalservice.utils.FilteringOrganizationBundleProvider;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents a resource provider for the FHIR Organization resource,
 *
 */
public class FHIROrganizationResourceProvider extends BaseJpaResourceProvider<Organization> {

    private static final Logger logger = LoggerFactory.getLogger(FHIROrganizationResourceProvider.class);
    private IFhirResourceDao<PractitionerRole> practitionerRoleDaoR4;
    private FilteringOrganizationBundleProvider filteringProvider;

    public FHIROrganizationResourceProvider(IFhirResourceDao<PractitionerRole> practitionerRoleDaoR4, IFhirResourceDao<Organization> dao, FhirContext fhirContext,
                                            FilteringOrganizationBundleProvider filteringProvider) {
        super(dao);
        this.filteringProvider = filteringProvider;
        setContext(fhirContext);
        this.practitionerRoleDaoR4 = practitionerRoleDaoR4;
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Organization> getResourceType() {
        return Organization.class;
    }

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
            RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public Organization read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

    /**
     * Returns Bundle of FHIR Organization resources that the practitioner with practitionerId is affiliated with.
     * If practitioner isn't affiliated with any organizations an empty Bundle is returned.
     * If no Practitioner resource can be found with a business identifier matching practitionerId an error is returned.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param thePractitioner The official (business) identifier of the practitioner (in Denmark a socalled authorization code)
     * @return Bundle of Organization resources
     */

    @Search(queryName = "getPractitionerOrganizations")
    public Bundle getPractitionerOrganizations(HttpServletRequest theRequest, RequestDetails theRequestDetails,
                                               HttpServletResponse theResponse,
            @RequiredParam(name= PractitionerRole.SP_PRACTITIONER) TokenParam thePractitioner) {
        logger.debug("getPractitionerOrganizations");
        Bundle retVal = new Bundle();

        try {
            this.startRequest(theRequest);

            SearchParameterMap searchParameterMap = new SearchParameterMap();
            searchParameterMap.add("practitionerIdentifier", thePractitioner);
            searchParameterMap.add("active", new TokenParam().setValue("true"));
            // searchParameterMap.setEverythingMode(SearchParameterMap.EverythingModeEnum.ENCOUNTER_INSTANCE);
            //NOTICE: For TEST-purposes, temporarily reintroducing code that makes HAPI throw error (calling without request + response objects)
            logger.debug("Calling DAO without request details");
            IBundleProvider practitionerRoleBundleProvider = practitionerRoleDaoR4.search(searchParameterMap);
            //IBundleProvider practitionerRoleBundleProvider = OrganizationalService.practitionerRoleDaoR4.search(searchParameterMap, theRequestDetails, theResponse);


            if(practitionerRoleBundleProvider.size() == 0){
                return retVal;
            }
            // create new parameter map for searching Organizations

            SearchParameterMap parameterMap = new SearchParameterMap();
            TokenOrListParam referenceOrListParam = new TokenOrListParam();
            List<IBaseResource> practitionerRoles = practitionerRoleBundleProvider.getResources(0, practitionerRoleBundleProvider.size());
            for (IBaseResource practitionerRole : practitionerRoles) {
                if(((PractitionerRole) practitionerRole).getActive()) {
                    Reference organizationTarget = ((PractitionerRole) practitionerRole).getOrganization();
                    referenceOrListParam.addOr(new TokenParam(
                            organizationTarget.getIdentifier().getSystem(),
                            organizationTarget.getIdentifier().getValue()
                    ));
                }
            }


            parameterMap.add("identifier",referenceOrListParam);
            IBundleProvider organizationBundleProvider = this.getDao().search(parameterMap,theRequestDetails,theResponse);

            if(organizationBundleProvider.size() == 0){
                return retVal;
            }

            List<IBaseResource> organizations = organizationBundleProvider.getResources(0,organizationBundleProvider.size());
            for (IBaseResource organization : organizations){
                String classString = organization.getClass().getName();
                if (classString.equals("org.hl7.fhir.r4.model.Organization")){
                    retVal.addEntry().setResource((Organization) organization);
                }
            }
        }
        finally {
           this.endRequest(theRequest);
        }
        return filteringProvider.filter(retVal);
    }

    /**
     * Get Organization resource by Identifier.
     *
     * Example invocation: http://fhir.example.com/Organization?identifier=urn:oid:1.2.208.176.1.1|373241000016009
     *
     * @param identifier Identifier of an Organization to search for (required)
     *
     * @return Organization with matching Identifier or null if not found
     */
    @Search(queryName = "getByIdentifier")
    public Bundle getByIdentifier(HttpServletRequest theRequest,
                                           HttpServletResponse theServletResponse,
                                           RequestDetails theRequestDetails,
                                           @RequiredParam(name=Organization.SP_IDENTIFIER) TokenParam identifier) {
        try {
            this.startRequest(theRequest);

            SearchParameterMap paramMap = new SearchParameterMap();
            paramMap.add("identifier", identifier);
            return filteringProvider.filter(this.getDao().search(paramMap, theRequestDetails, theServletResponse));

        } finally {
            this.endRequest(theRequest);
        }
    }

    /**
     * Gets Organizations that has the given identifier in their partOf field. That is, the children of the Organization given.
     * @param theRequest the request
     * @param theServletResponse the response
     * @param theRequestDetails the details
     * @param identifier the identifier of the Organization which children you want to find
     * @return a Bundle containing the children of the given Organization.
     */

    @Search(queryName = "getChildren")
    public IBundleProvider getChildren(HttpServletRequest theRequest,
                                           HttpServletResponse theServletResponse,
                                           RequestDetails theRequestDetails,
                                           @RequiredParam(name="partOfIdentifier") TokenParam identifier) {
        try {
            this.startRequest(theRequest);

            SearchParameterMap paramMap = new SearchParameterMap();
            paramMap.add("partOfIdentifier", identifier);
            return this.getDao().search(paramMap, theRequestDetails, theServletResponse);

        } finally {
            this.endRequest(theRequest);
        }
    }

    @Search(queryName = "getDescendants")
    public Bundle getDescendants(HttpServletRequest theRequest,
                                          HttpServletResponse theServletResponse,
                                          RequestDetails theRequestDetails,
                                          @RequiredParam(name="partOfIdentifier") TokenParam identifier) {
        Bundle returnBundle = new Bundle();
        try{
            this.startRequest(theRequest);

            findDescendantsAndAddToResultBundle(theServletResponse,theRequestDetails,identifier,returnBundle);

        } finally {
            this.endRequest(theRequest);
        }

        return filteringProvider.filter(returnBundle);
    }

    protected void findDescendantsAndAddToResultBundle(HttpServletResponse theServletResponse,
                                                       RequestDetails theRequestDetails,
                                                       TokenParam identifier,
                                                       Bundle returnBundle){
        List<IBaseResource> organizations = getOrganizationsMatchingPartOfIdentifier(identifier, theRequestDetails, theServletResponse);

        if(organizations.isEmpty()) return;

        for (IBaseResource organization : organizations){
            returnBundle.addEntry().setResource((Organization) organization);

            findDescendantsAndAddToResultBundle(theServletResponse,theRequestDetails,
                    getOrganizationIdentifierTokenParam((Organization) organization),
                    returnBundle);
        }
    }

    protected TokenParam getOrganizationIdentifierTokenParam(Organization organization) {
        return new TokenParam(organization.getIdentifierFirstRep().getSystem(),
                        organization.getIdentifierFirstRep().getValue());
    }

    protected List<IBaseResource> getOrganizationsMatchingPartOfIdentifier(TokenParam identifier, RequestDetails theRequestDetails, HttpServletResponse theServletResponse){
        SearchParameterMap searchParameterMap = new SearchParameterMap();
        searchParameterMap.add("partOfIdentifier", identifier);
        IBundleProvider organizationIBundleProvider = getDao().search(searchParameterMap, theRequestDetails, theServletResponse);

        return organizationIBundleProvider.getResources(0,organizationIBundleProvider.size());
    }

    /**
     *
     * Generic search API for retrieval of Bundles of Organizations stored by this service.
     *
     * @param theServletRequest
     * @param theServletResponse
     * @param theRequestDetails
     * @param theFtContent Search the contents of the resource's data using a fulltext search
     * @param theHas Return resources linked to by the given target
     * @param the_id The ID of the resource
     * @param theActive Is the Organization record active
     * @param theAddress A server defined search that may match any of the string fields in the Address, including line, city, district, country, postalCode, and/or text
     * @param theAddress_city A city specified in an address
     * @param theAddress_country A country specified in an address
     * @param theAddress_postalcode A postal code specified in an address
     * @param theIdentifier Any identifier for the organization
     * @param theName A portion of the organization's name or alias
     * @param thePartof An organization of which this organization forms a part
     * @param theType A code for the type of organization
     * @param theLastUpdated Only return resources which were last updated as specified by the given range
     * @param theSort sort order
     * @param theCount
     * @param theSummaryMode
     * @param theSearchTotalMode
     * @return Bundle of Organization resources
     *
     */
    @Search()
    public Bundle search(HttpServletRequest theServletRequest,
            HttpServletResponse theServletResponse,
            RequestDetails theRequestDetails,
            @Description(shortDefinition = "Search the contents of the resource's data using a fulltext search")
            @OptionalParam(name = "_content") StringAndListParam theFtContent,
            @Description(shortDefinition = "Return resources linked to by the given target")
            @OptionalParam(name = "_has") HasAndListParam theHas,
            @Description(shortDefinition = "The ID of the resource")
            @OptionalParam(name = "_id") TokenAndListParam the_id,
            @Description(shortDefinition = "Is the Organization record active")
            @OptionalParam(name = "active") TokenAndListParam theActive,
            @Description(shortDefinition = "A server defined search that may match any of the string fields in the Address, including line, city, district, state, country, postalCode, and/or text")
            @OptionalParam(name = "address") StringAndListParam theAddress,
            @Description(shortDefinition = "A city specified in an address")
            @OptionalParam(name = "address-city") StringAndListParam theAddress_city,
            @Description(shortDefinition = "A country specified in an address")
            @OptionalParam(name = "address-country") StringAndListParam theAddress_country,
            @Description(shortDefinition = "A postal code specified in an address")
            @OptionalParam(name = "address-postalcode") StringAndListParam theAddress_postalcode,
            @Description(shortDefinition = "Any identifier for the organization")
            @OptionalParam(name = "identifier") TokenAndListParam theIdentifier,
            @Description(shortDefinition = "A portion of the organization's name or alias")
            @OptionalParam(name = "name") StringAndListParam theName,
            @Description(shortDefinition = "An organization of which this organization forms a part")
            @OptionalParam(name = "partof",targetTypes = {}) ReferenceAndListParam thePartof,
            @Description(shortDefinition = "A code for the type of organization")
            @OptionalParam(name = "type") TokenAndListParam theType,
            @Description(shortDefinition = "Only return resources which were last updated as specified by the given range")
            @OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,
            @Sort SortSpec theSort,
            @Count Integer theCount,
            SummaryEnum theSummaryMode,
            SearchTotalModeEnum theSearchTotalMode) {

        logger.debug("search");

        this.startRequest(theServletRequest);

        IBundleProvider iBundleProvider;
        try {
            SearchParameterMap paramMap = new SearchParameterMap();
            paramMap.add("_content", theFtContent);
            paramMap.add("_has", theHas);
            paramMap.add("_id", the_id);
            paramMap.add("active", theActive);
            paramMap.add("address", theAddress);
            paramMap.add("address-city", theAddress_city);
            paramMap.add("address-country", theAddress_country);
            paramMap.add("address-postalcode", theAddress_postalcode);
            paramMap.add("identifier", theIdentifier);
            paramMap.add("name", theName);
            paramMap.add("partof", thePartof);
            paramMap.add("type", theType);
            paramMap.setLastUpdated(theLastUpdated);
            paramMap.setSort(theSort);
            paramMap.setCount(theCount);
            paramMap.setSummaryMode(theSummaryMode);
            paramMap.setSearchTotalMode(theSearchTotalMode);
            IBundleProvider retVal = this.getDao().search(paramMap, theRequestDetails, theServletResponse);
            iBundleProvider = retVal;
        } finally {
            this.endRequest(theServletRequest);
        }

        return filteringProvider.filter(iBundleProvider);
    }
}
