package dk.s4.microservices.organizationalservice.servlet;

import static dk.s4.microservices.microservicecommon.fhir.ResourceUtil.getResourceStream;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.search.reindex.IResourceReindexingSvc;
import ca.uhn.fhir.jpa.searchparam.registry.ISearchParamRegistry;
import ca.uhn.fhir.parser.StrictErrorHandler;
import ca.uhn.fhir.rest.server.IResourceProvider;
import dk.s4.microservices.genericresourceservice.servlet.JpaServerGenericServlet;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.microservicecommon.Env;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.MetricsEventConsumerInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.fhir.SearchParameterFacade;
import dk.s4.microservices.microservicecommon.security.*;
import dk.s4.microservices.organizationalservice.health.HealthEndpoint;
import dk.s4.microservices.organizationalservice.messaging.MyEventProcessor;
import dk.s4.microservices.organizationalservice.provider.FHIROrganizationResourceProvider;
import dk.s4.microservices.organizationalservice.provider.FHIRPractitionerResourceProvider;
import dk.s4.microservices.organizationalservice.provider.FHIRPractitionerRoleResourceProvider;
import dk.s4.microservices.organizationalservice.utils.FilteringConsumer;
import dk.s4.microservices.organizationalservice.utils.FilteringOrganizationBundleProvider;
import dk.s4.microservices.organizationalservice.utils.SORDataRetriever;
import dk.s4.microservices.organizationalservice.utils.SORHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.SearchParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

/**
 * Organizational Service.
 * <p>
 * It exposes a RESTful interface for FHIR Organization, Practitioner and PractitionerRole resources.
 */
public class OrganizationalService extends JpaServerGenericServlet {

    private static final long serialVersionUID = 1L;

    private final static Logger logger = LoggerFactory.getLogger(OrganizationalService.class);
    private static FhirContext fhirContext;
    private static MyEventProcessor eventProcessor;
    private static List<Topic> topics;
    private static IResourceReindexingSvc reindexing;

    private KafkaConsumeAndProcess kafkaConsumeAndProcess;
    private Thread kafkaConsumeAndProcessThread;
    private UserContextResolverInterface userResolver;

    private static IFhirResourceDao<Organization> organizationDaoR4;
    private static IFhirResourceDao<Practitioner> practitionerDaoR4;
    private static IFhirResourceDao<PractitionerRole> practitionerRoleDaoR4;

    public OrganizationalService() {
        super(getMyFhirContext(), logger);
    }

    /**
     * Singleton FhirContext
     *
     * @return the FhirContext
     */
    private static FhirContext getMyFhirContext() {
        if (fhirContext == null) {
            fhirContext = FhirContext.forR4();
            fhirContext.setParserErrorHandler(new StrictErrorHandler());
        }
        return fhirContext;
    }

    @Override
    public UserContextResolverInterface getUserResolver() {
        if(userResolver == null) {
            if (System.getenv("ENABLE_DIAS_AUTHENTICATION").equals("true")) {
                userResolver = new DiasUserContextResolver(System.getenv("USER_CONTEXT_SERVICE_URL"));
            } else if (System.getenv("ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION").equals("true")) {
                userResolver = new KeycloakGatekeeperUserContextResolver();
            } else if (System.getenv("ENABLE_OAUTH2_PROXY_AUTHORIZATION").equals("true")) {
                userResolver = new OAuth2ProxyUserContextResolver();
            } else {
                userResolver = new DefaultUserContextResolver();
            }
        }
        return userResolver;
    }

    @Override
    public void destroy() {
        try {
            if (kafkaConsumeAndProcess != null) {
                kafkaConsumeAndProcess.stopThread();
            }
            if (kafkaConsumeAndProcessThread != null) {
                kafkaConsumeAndProcessThread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void registerAndCheckEnvironmentVars() {
        Env.registerRequiredEnvVars(Arrays.asList(
                "SERVICE_NAME",
                "FHIR_VERSION",
                "CORRELATION_ID",
                "TRANSACTION_ID",
                "LOG_LEVEL",
                "LOG_LEVEL_DK_S4",
                "ENABLE_KAFKA",
                "INITIALIZE_SEARCH_PARAM",
                "RETRIEVE_SOR",
                "ENABLE_SOR",
                "SOR_ENTITY_TYPE_IDENTIFIER",
                "DATABASE_URL",
                "DATABASE_USERNAME",
                "DATABASE_PASSWORD",
                "MYSQL_ALLOW_EMPTY_PASSWORD",
                "MYSQL_USER",
                "MYSQL_PASSWORD",
                "ENABLE_AUTH",
                "OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM",
                "OFFICIAL_PRACTITIONER_IDENTIFIER_SYSTEM",
                "OFFICIAL_PRACTITIONERROLE_IDENTIFIER_SYSTEM",
                "SOR_BLACKLIST"
        ));
        Env.registerConditionalEnvVars("ENABLE_KAFKA",
                Arrays.asList(
                        "KAFKA_BOOTSTRAP_SERVER",
                        "KAFKA_KEY_DESERIALIZER",
                        "KAFKA_VALUE_DESERIALIZER",
                        "KAFKA_ENABLE_AUTO_COMMIT",
                        "KAFKA_AUTO_COMMIT_INTERVAL_MS",
                        "KAFKA_SESSION_TIMEOUT_MS",
                        "KAFKA_GROUP_ID",
                        "KAFKA_ACKS",
                        "KAFKA_RETRIES",
                        "KAFKA_KEY_SERIALIZER",
                        "KAFKA_VALUE_SERIALIZER",
                        "ENABLE_HEALTH",
                        "HEALTH_FILE_PATH",
                        "HEALTH_INTERVAL_MS")
        );
    }

    @Override
    protected void initialize() throws ServletException {
        super.initialize();
        registerAndCheckEnvironmentVars();

        logger.debug("initialize");

        // Get the spring context from the web container (it's declared in web.xml)
        WebApplicationContext myAppCtx = ContextLoaderListener.getCurrentWebApplicationContext();

        organizationDaoR4 = myAppCtx.getBean("myOrganizationDaoR4", IFhirResourceDao.class);
        practitionerDaoR4 = myAppCtx.getBean("myPractitionerDaoR4", IFhirResourceDao.class);
        practitionerRoleDaoR4 = myAppCtx.getBean("myPractitionerRoleDaoR4", IFhirResourceDao.class);
        IFhirResourceDao<SearchParameter> searchParameterDaoR4 = myAppCtx.getBean("mySearchParameterDaoR4", IFhirResourceDao.class);
        reindexing = myAppCtx.getBean(IResourceReindexingSvc.class);

        //Initiate Custom SearchParameters
        SearchParameterFacade searchParameterFacade = new SearchParameterFacade(searchParameterDaoR4, getMyFhirContext());
        try {
            searchParameterFacade.installSearchParameter(getResourceStream("PractitionerRoleOrganizationIdentifierSearchParameter.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("PractitionerRolePractitionerIdentifierSearchParameter.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("OrganizationPartOfSearchParameter.json"));
            myAppCtx.getBean(ISearchParamRegistry.class).forceRefresh();
        } catch (IOException e) {
            throw new InternalError("Something went wrong while reading the custom SearchParameters");
        }

        boolean retrieveSorData = false;
        String retrieveSorDataString = System.getenv("RETRIEVE_SOR");
        logger.debug("Read RETRIEVE_SOR from environment: " + retrieveSorDataString);
        if (retrieveSorDataString != null && retrieveSorDataString.equals("true")) {
            retrieveSorData = true;
        }
        boolean enableSorData = false;
        String enableSorDataString = System.getenv("ENABLE_SOR");
        logger.debug("Read ENABLE_SOR from environment: " + enableSorDataString);
        if (enableSorDataString != null && enableSorDataString.equals("true")) {
            enableSorData = true;
        }

        String errorString = "";
        try {
            if (retrieveSorData) {
                errorString = "first";
                SORDataRetriever sorDataRetriever = new SORDataRetriever();
                sorDataRetriever.retrieveSorAndExtract();
            }
            if (enableSorData) {
                errorString = "second";
                SORHandler sorHandler = new SORHandler(new FilteringConsumer(organizationDaoR4));
                sorHandler.parse();
            }
        } catch (Exception e) {
            logger.error("Error while populating Dao from SorXML file: " + errorString, e);
        }

        List<IResourceProvider> resourceProviders = new ArrayList<>();
        resourceProviders.add(new FHIROrganizationResourceProvider(practitionerRoleDaoR4, organizationDaoR4, getMyFhirContext(),
                new FilteringOrganizationBundleProvider(System.getenv("SOR_BLACKLIST"))));
        resourceProviders.add(new FHIRPractitionerResourceProvider(practitionerRoleDaoR4, practitionerDaoR4, getMyFhirContext()));
        resourceProviders.add(new FHIRPractitionerRoleResourceProvider(practitionerRoleDaoR4, getMyFhirContext()));
        setResourceProviders(resourceProviders);

        topics = Arrays.asList(
                FhirTopics.create("Organization"),
                FhirTopics.update("Organization"),

                FhirTopics.create("Practitioner"),
                FhirTopics.update("Practitioner"),

                FhirTopics.create("PractitionerRole"),
                FhirTopics.update("PractitionerRole"));

        if (System.getenv("ENABLE_KAFKA").equals("true")) {
            try {
                KafkaEventProducer kafkaEventProducer = new KafkaEventProducer(System.getenv("SERVICE_NAME"));
                eventProcessor = new MyEventProcessor(getMyFhirContext(), organizationDaoR4, practitionerDaoR4, practitionerRoleDaoR4);
                kafkaConsumeAndProcess = new KafkaConsumeAndProcess(topics, kafkaEventProducer, eventProcessor);
                kafkaConsumeAndProcess.registerInterceptor(new MetricsEventConsumerInterceptorAdaptor());
                if (Env.isSetToTrue("ENABLE_DIAS_AUTHENTICATION")) {
                    kafkaConsumeAndProcess.registerInterceptor(new DiasEventConsumerInterceptor());
                }
                kafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess, "Kafka-process-thread");
                kafkaConsumeAndProcessThread.start();
                HealthEndpoint.registerKafkaConsumeAndProcess(kafkaConsumeAndProcess);
            } catch (KafkaInitializationException | MessagingInitializationException e) {
                logger.error("Error during Kafka initialization: ", e);
            }
        }
    }

    /**
     * For testing
     */
    static FhirContext getServerFhirContext() {
        return getMyFhirContext();
    }

    /**
     * For testing
     */
    static IFhirResourceDao<Organization> getOrganizationDao() {
        if (organizationDaoR4 != null) {
            return organizationDaoR4;
        }
        throw new RuntimeException("Organization dao not initialized");
    }

    /**
     * For testing
     */
    static IFhirResourceDao<Practitioner> getPractitionerDao() {
        if (practitionerDaoR4 != null) {
            return practitionerDaoR4;
        }
        throw new RuntimeException("Practitioner dao not initialized");
    }

    /**
     * For testing
     */
    static IFhirResourceDao<PractitionerRole> getPractitionerRoleDao() {
        if (practitionerRoleDaoR4 != null) {
            return practitionerRoleDaoR4;
        }
        throw new RuntimeException("PractitionerRole dao not initialized");
    }

    /**
     * For testing
     */
    static List<Topic> getTopics() {
        if (topics != null) {
            return topics;
        }
        throw new RuntimeException("Topics not initialized");
    }

    /**
     * For testing
     */
    static MyEventProcessor getEventProcessor() {
        if (eventProcessor != null) {
            return eventProcessor;
        }
        throw new RuntimeException("Event processor not initialized");
    }

    /**
     * For testing
     */
    static void forceReindexing() {
        if (reindexing != null) {
            reindexing.forceReindexingPass();
        }
        else {
            logger.error("Reindexing failed");
        }
    }
}
