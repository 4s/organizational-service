package dk.s4.microservices.organizationalservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.IQueryParameterType;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.organizationalservice.utils.FilteringOrganizationBundleProvider;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIROrganizationResourceProviderTest {

    private HttpServletResponse response;
    private RequestDetails details;
    private Organization org1;
    private Organization org2;
    private Organization org3;
    private Organization org4;
    private Organization org5;
    private Organization org6;
    private Organization org7;
    private Organization org8;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setup() {
        environmentVariables.set("SOR_BLACKLIST", "identifier_one,identifier_two,identifier_three");
        response = mock(HttpServletResponse.class);
        details = mock(RequestDetails.class);
    }

    @Test
    public void findDescendantsNoDescendants() {
        FHIROrganizationResourceProvider provider = getProviderWithOrganizations();

        Bundle returnBundle = new Bundle();
        provider.findDescendantsAndAddToResultBundle(response, details, provider.getOrganizationIdentifierTokenParam(org8), returnBundle);

        assertThat(returnBundle.isEmpty()).isTrue();
    }

    @Test
    public void findDescendantsOneDescendants() {
        FHIROrganizationResourceProvider provider = getProviderWithOrganizations();

        Bundle returnBundle = new Bundle();
        provider.findDescendantsAndAddToResultBundle(response, details, provider.getOrganizationIdentifierTokenParam(org3), returnBundle);

        assertThat(returnBundle.isEmpty()).isFalse();
        assertThat(returnBundle.getEntry().size()).isEqualTo(1);
    }

    @Test
    public void findDescendantsTwoDescendants() {
        FHIROrganizationResourceProvider provider = getProviderWithOrganizations();

        Bundle returnBundle = new Bundle();
        provider.findDescendantsAndAddToResultBundle(response, details, provider.getOrganizationIdentifierTokenParam(org4), returnBundle);

        assertThat(returnBundle.isEmpty()).isFalse();
        assertThat(returnBundle.getEntry().size()).isEqualTo(2);
    }

    @Test
    public void findDescendantsFourDescendants() {
        FHIROrganizationResourceProvider provider = getProviderWithOrganizations();

        Bundle returnBundle = new Bundle();
        provider.findDescendantsAndAddToResultBundle(response, details, provider.getOrganizationIdentifierTokenParam(org2), returnBundle);

        assertThat(returnBundle.isEmpty()).isFalse();
        assertThat(returnBundle.getEntry().size()).isEqualTo(4);
    }

    @Test
    public void findDescendantsSevenDescendants() {
        FHIROrganizationResourceProvider provider = getProviderWithOrganizations();

        Bundle returnBundle = new Bundle();
        provider.findDescendantsAndAddToResultBundle(response, details, provider.getOrganizationIdentifierTokenParam(org1), returnBundle);

        assertThat(returnBundle.isEmpty()).isFalse();
        assertThat(returnBundle.getEntry().size()).isEqualTo(7);
    }

    @Test
    public void getDescendants() {
        FHIROrganizationResourceProvider provider = getProviderWithOrganizations();

        Bundle returnBundle = provider.getDescendants(null, response, details, new TokenParam().setSystem("theOrganizationIdentifierSystem").setValue("org1"));

        assertThat(returnBundle.isEmpty()).isFalse();
        assertThat(returnBundle.getEntry().size()).isEqualTo(7);
    }

    @Test
    public void getOrganizationIdentifierTokenParam() {
        IFhirResourceDao<Organization> dao = getDaoWithOrganizations();
        FHIROrganizationResourceProvider provider = new FHIROrganizationResourceProvider(mock(IFhirResourceDao.class), dao, mock(FhirContext.class),
                new FilteringOrganizationBundleProvider(System.getenv("SOR_BLACKLIST")));

        Organization organization = mock(Organization.class);
        Identifier identifier = new Identifier().setValue("theValue").setSystem("theSystem");
        when(organization.getIdentifierFirstRep()).thenReturn(identifier);

        TokenParam tokenParam = provider.getOrganizationIdentifierTokenParam(organization);

        assertThat(tokenParam.getValue()).isEqualTo("theValue");
        assertThat(tokenParam.getSystem()).isEqualTo("theSystem");
    }

    @Test
    public void getOrganizationsMatchingPartOfIdentifier() {
        IFhirResourceDao<Organization> dao = mock(IFhirResourceDao.class);
        AtomicReference<SearchParameterMap> searchParameterMap = new AtomicReference<>();

        when(dao.search(any(), any(), any())).then(invocationOnMock -> {
            searchParameterMap.set((SearchParameterMap) invocationOnMock.getArguments()[0]);
            return mock(IBundleProvider.class);
        });
        FHIROrganizationResourceProvider provider = new FHIROrganizationResourceProvider(mock(IFhirResourceDao.class), dao, mock(FhirContext.class),
                new FilteringOrganizationBundleProvider(System.getenv("SOR_BLACKLIST")));

        TokenParam identifier = new TokenParam().setValue("theValue").setSystem("theSystem");
        provider.getOrganizationsMatchingPartOfIdentifier(identifier, mock(RequestDetails.class), mock(HttpServletResponse.class));

        assertThat(getTokenParamFrom(searchParameterMap.get().get("partOfIdentifier")).getValue()).isEqualTo("theValue");
        assertThat(getTokenParamFrom(searchParameterMap.get().get("partOfIdentifier")).getSystem()).isEqualTo("theSystem");
    }

    private TokenParam getTokenParamFrom(List<List<? extends IQueryParameterType>> list) {
        return (TokenParam) list.get(0).get(0);
    }


    /**
     * Constructs a dao with a the following tree of organizations
     * <p>
     *                org1
     *              /     \
     *           org2     org3
     *         /     \        \
     *       org4    org5      org6
     *     /    \
     *  org7    org8
     *
     * @return the dao
     */

    private IFhirResourceDao<Organization> getDaoWithOrganizations() {

        IFhirResourceDao<Organization> dao = mock(IFhirResourceDao.class);
        org1 = organizationWithIdentifier("org1");
        org2 = organizationWithIdentifier("org2");
        org3 = organizationWithIdentifier("org3");
        org4 = organizationWithIdentifier("org4");
        org5 = organizationWithIdentifier("org5");
        org6 = organizationWithIdentifier("org6");
        org7 = organizationWithIdentifier("org7");
        org8 = organizationWithIdentifier("org8");

        IBundleProvider org1BundleProvider = bundleProviderWith(org2, org3);
        IBundleProvider org2BundleProvider = bundleProviderWith(org4, org5);
        IBundleProvider org3BundleProvider = bundleProviderWith(org6);
        IBundleProvider org4BundleProvider = bundleProviderWith(org7, org8);
        IBundleProvider EmptyBundleProvider = bundleProviderWith();

        when(dao.search(any(), any(), any())).then(invocationOnMock -> {
            SearchParameterMap searchParameterMap = (SearchParameterMap) invocationOnMock.getArguments()[0];
            String string = searchParameterMap.get("partOfIdentifier").get(0).get(0).toString();

            if (string.contains("org1")) {
                return org1BundleProvider;
            } else if (string.contains("org2")) {
                return org2BundleProvider;
            } else if (string.contains("org3")) {
                return org3BundleProvider;
            } else if (string.contains("org4")) {
                return org4BundleProvider;
            } else return EmptyBundleProvider;
        });

        return dao;
    }

    private IBundleProvider bundleProviderWith(Organization... organizations) {
        IBundleProvider provider = mock(IBundleProvider.class);
        when(provider.getResources(anyInt(), anyInt())).thenReturn(Arrays.asList(organizations));
        return provider;
    }

    private Organization organizationWithIdentifier(String identifier) {
        Organization organization = new Organization();
        organization.setIdentifier(constructIdentifier(identifier));
        return organization;
    }

    private List<Identifier> constructIdentifier(String theIdentifier) {
        List<Identifier> identifiers = new ArrayList<>();
        identifiers.add(new Identifier().setValue(theIdentifier).setSystem("theOrganizationIdentifierSystem"));
        return identifiers;
    }

    private FHIROrganizationResourceProvider getProviderWithOrganizations() {
        return new FHIROrganizationResourceProvider(mock(IFhirResourceDao.class), getDaoWithOrganizations(), mock(FhirContext.class),
                new FilteringOrganizationBundleProvider(System.getenv("SOR_BLACKLIST")));
    }

}