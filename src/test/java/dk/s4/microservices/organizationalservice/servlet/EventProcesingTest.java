package dk.s4.microservices.organizationalservice.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Message.BodyCategory;
import dk.s4.microservices.messaging.Message.Prefer;
import dk.s4.microservices.messaging.MessageParseException;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import dk.s4.microservices.organizationalservice.messaging.MyEventProcessor;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Organization;
import org.json.simple.parser.ParseException;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class EventProcesingTest {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(EventProcesingTest.class);

    private static long FUTURE_TIMEOUT = 100000;
    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;

    private static IGenericClient ourClient;
    private static FhirContext ourCtx = FhirContext.forR4();
    private static IParser jsonParser;

    private static int ourPort;

    private static Server ourServer;
    private static String ourServerBase;

    private static String ENVIRONMENT_FILE = "service.env";
    private static String KAFKA_PRODUCER_PROPS = "KafkaProducer.props";


    @ClassRule
    public static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testCreateOrganization()
            throws IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException {
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Organization.json");

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(resourceString);
        message.setSender("EventProcesingTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("Organization");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        logger.trace("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR)
                .setDataType("Organization");
        ProducerRecord<String, String> record =
                new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        //Test creation of Observation with code MDC188736
        Topic outputTopic = new Topic()
                .setOperation(Operation.DataCreated)
                .setDataCategory(Category.FHIR)
                .setDataType("Organization");

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("Organization", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertEquals("1", outputMessage.getEtagVersion());

        assertNotNull(metadata);
    }

    @Test
    public void testUpdateOrganization()
            throws IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException {
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Organization.json");

        IParser parser = OrganizationalService.getServerFhirContext().newJsonParser();
        DaoMethodOutcome outcome = OrganizationalService.getOrganizationDao().create(
                parser.parseResource(Organization.class, resourceString));
        Organization org = OrganizationalService.getOrganizationDao().read(outcome.getId());
        org.addAlias("testUpdateOrganization-alias");

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(parser.encodeResourceToString(org));
        message.setSender("EventProcesingTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("Organization");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        logger.trace("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Update)
                .setDataCategory(Category.FHIR)
                .setDataType("Organization");
        ProducerRecord<String, String> record =
                new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        //Test creation of Observation with code MDC188736
        Topic outputTopic = new Topic()
                .setOperation(Operation.DataUpdated)
                .setDataCategory(Category.FHIR)
                .setDataType("Organization");

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("Organization", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertEquals("2", outputMessage.getEtagVersion());

        Organization outputOrg =  parser.parseResource(Organization.class, message.getBody());
        String alias = outputOrg.getAlias().get(outputOrg.getAlias().size()-1).toString();
        assertEquals("testUpdateOrganization-alias", alias);

        assertNotNull(metadata);
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        //Use Derby local file based database
        String value = System.getenv("DATABASE_TYPE");
        if (value == null)
            environmentVariables.set("DATABASE_TYPE", "Derby");

        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = EventProcesingTest.class.getClassLoader().getResource(".keep").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        logger.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        TestUtils.readEnvironment(path + "/" + ENVIRONMENT_FILE, environmentVariables);
        TestUtils.readEnvironment(path + "/deploy.env", environmentVariables);
        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer(path+"/src/main/webapp/WEB-INF/without-keycloak/web.xml",path+"/target/service", environmentVariables, ourPort, ourServer);

        // Instantiate mock Kafka classes
        mockExternalKafkaProducer = TestUtils.initMockExternalProducer();
        MyEventProcessor eventProcessor = new MyEventProcessor(OrganizationalService.getServerFhirContext(),
                OrganizationalService.getOrganizationDao(),
                OrganizationalService.getPractitionerDao(),
                OrganizationalService.getPractitionerRoleDao());
        mockKafkaEventProducer = TestUtils.initMockKafkaConsumeAndProcess(OrganizationalService.getTopics(), eventProcessor);
    }
}
