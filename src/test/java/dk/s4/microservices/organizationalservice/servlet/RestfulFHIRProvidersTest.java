package dk.s4.microservices.organizationalservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.PreferReturnEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.*;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class RestfulFHIRProvidersTest {

    private static final Logger logger = LoggerFactory.getLogger(RestfulFHIRProvidersTest.class);

    private static IGenericClient ourClient;
    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(RestfulFHIRProvidersTest.class);
    private static IParser jsonParser;

    private static int ourPort;

    private static Server ourServer;
    private static String ourServerBase;

    private static String ENVIRONMENT_FILE = "service.env";

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testOrganizationPatchNotSupported() {
        NotImplementedOperationException expectedException = null;

        try {
            MethodOutcome methodOutcome = ourClient.patch().
                    withBody("[ { \"op\":\"replace\", \"path\":\"/active\", \"value\":false } ]").
                    withId("Organization/1").prefer(PreferReturnEnum.OPERATION_OUTCOME).execute();
        } catch (NotImplementedOperationException e) {
            expectedException = e;
        }

        Assert.assertNotNull(expectedException);
    }

    @Test
    public void testOrganizationCreateNotSupported() {
        InvalidRequestException expectedException = null;

        Organization organization = new Organization();
        organization.setName("testOrganizationPatch");
        try {
            MethodOutcome methodOutcome = ourClient.create().resource(organization).execute();
        } catch (InvalidRequestException e) {
            expectedException = e;
        }

        Assert.assertNotNull(expectedException);
    }

    @Test
    public void testOrganizationSearch() {
        String searchUrl = ourServerBase + "/Organization?";//"/Organization?name=test";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetChildren() {
        String searchUrl = ourServerBase + "/Organization?_query=getChildren"
                + "&partOfIdentifier=urn:oid:1.2.208.176.1.1|404261000016009";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testPractitionerSearch() {
        String searchUrl = ourServerBase + "/Practitioner?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testPractitionerRoleSearch() {
        String searchUrl = ourServerBase + "/PractitionerRole?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetOrganizationPractitionersTenTimes() {
        String searchUrl = ourServerBase + "/Practitioner?_query=getOrganizationPractitioners&organization=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = new Bundle();

        for (int i = 0; i < 10; i++) {
            result = query.execute();
        }

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetOrganizationPractitioners() {
        String searchUrl = ourServerBase + "/Practitioner?_query=getOrganizationPractitioners&organization=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testSpecificPractitionerRoleSearch() {
        String searchUrl = ourServerBase + "/PractitionerRole?organizationIdentifier=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }


    @Test
    public void testDaoSearch() {
        //Test searching for organization.identifier
        String searchUrl = ourServerBase + "/PractitionerRole?organizationIdentifier=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();
        Assert.assertTrue(result.hasEntry());

        //Test searching for practitioner.identifier
        searchUrl = ourServerBase + "/PractitionerRole?practitionerIdentifier=https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/|55GT7";
        query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetPractitionerOrganizations() {
        String searchUrl = ourServerBase + "/Organization?_query=getPractitionerOrganizations&practitioner=https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/|55GT7";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetPractitionerOrganizationsWrongId() {
        String searchUrl = ourServerBase + "/Organization?_query=getPractitionerOrganizations&practitioner=https://stps.dk/da/sundhedsprofessionelle-og-myndigheder/autorisationsregister/autorisationsid/|FWQFJOIWRI";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testGetDescendants() throws IOException {
        createTreeOfOrganizations();
        String searchUrl = ourServerBase + "/Organization?_query=getDescendants&partOfIdentifier=theOrganizationIdentifierSystem|parent-identifier";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());

        List<String> identifiersInResult = new ArrayList<>();
        for (Bundle.BundleEntryComponent entry : result.getEntry()) {
            identifiersInResult.add(((Organization) entry.getResource()).getIdentifierFirstRep().getValue());
        }

        assertThat(identifiersInResult).contains("first-child-identifier");
        assertThat(identifiersInResult).contains("first-child-first-child-identifier");
        assertThat(identifiersInResult).contains("first-child-second-child-identifier");
        assertThat(identifiersInResult).contains("second-child-identifier");
        assertThat(identifiersInResult).contains("third-child-identifier");

        assertThat(identifiersInResult).doesNotContain("453191000016003");
    }

    private void createTreeOfOrganizations() throws IOException {
        Organization organization = jsonParser.parseResource(Organization.class, ResourceUtil.stringFromResource("FHIR-clean/Organization.json"));
        organization.setIdentifier(constructIdentifier("parent-identifier"));
        OrganizationalService.getOrganizationDao().create(organization);

        organization.setIdentifier(constructIdentifier("first-child-identifier"));
        organization.setPartOf(constructReference("parent-identifier"));
        OrganizationalService.getOrganizationDao().create(organization);

        organization.setIdentifier(constructIdentifier("first-child-first-child-identifier"));
        organization.setPartOf(constructReference("first-child-identifier"));
        OrganizationalService.getOrganizationDao().create(organization);

        organization.setIdentifier(constructIdentifier("first-child-second-child-identifier"));
        organization.setPartOf(constructReference("first-child-identifier"));
        OrganizationalService.getOrganizationDao().create(organization);

        organization.setIdentifier(constructIdentifier("second-child-identifier"));
        organization.setPartOf(constructReference("parent-identifier"));
        OrganizationalService.getOrganizationDao().create(organization);

        organization.setIdentifier(constructIdentifier("third-child-identifier"));
        organization.setPartOf(constructReference("parent-identifier"));
        OrganizationalService.getOrganizationDao().create(organization);
    }

    private Reference constructReference(String identifier) {
        return new Reference().setIdentifier(new Identifier().setValue(identifier).setSystem("theOrganizationIdentifierSystem"))
                .setType("Organization");
    }

    private List<Identifier> constructIdentifier(String theIdentifier) {
        List<Identifier> identifiers = new ArrayList<>();
        identifiers.add(new Identifier().setValue(theIdentifier).setSystem("theOrganizationIdentifierSystem"));
        return identifiers;
    }

    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        String value = System.getenv("DATABASE_TYPE");
        if (value == null)
            environmentVariables.set("DATABASE_TYPE", "Derby");

        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = RestfulFHIRProvidersTest.class.getClassLoader().getResource(".keep").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        ourLog.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        TestUtils.readEnvironment(path + "/" + ENVIRONMENT_FILE, environmentVariables);
        TestUtils.readEnvironment(path + "/deploy.env", environmentVariables);
        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_SOR", "false");

        //Init web server and start it
        ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer(path + "/src/main/webapp/WEB-INF/without-keycloak/web.xml", path + "/target/service", environmentVariables, ourPort, ourServer);

        OrganizationalService.getServerFhirContext().getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
        OrganizationalService.getServerFhirContext().getRestfulClientFactory().setSocketTimeout(1200 * 1000);
        FhirContext fhirContext = OrganizationalService.getServerFhirContext();
        ourClient = fhirContext.newRestfulGenericClient(ourServerBase);
        ourClient.registerInterceptor(new LoggingInterceptor(true));

        jsonParser = OrganizationalService.getServerFhirContext().newJsonParser();

        String resourceString;

        resourceString = ResourceUtil.stringFromResource("FHIR-clean/Organization.json");
        OrganizationalService.getOrganizationDao().create(jsonParser.parseResource(Organization.class, resourceString));

        resourceString = ResourceUtil.stringFromResource("FHIR-clean/Practitioner.json");
        OrganizationalService.getPractitionerDao().create(jsonParser.parseResource(Practitioner.class, resourceString));

        resourceString = ResourceUtil.stringFromResource("FHIR-clean/PractitionerRole.json");
        OrganizationalService.getPractitionerRoleDao().create(jsonParser.parseResource(PractitionerRole.class, resourceString));
        OrganizationalService.forceReindexing();

    }


}
