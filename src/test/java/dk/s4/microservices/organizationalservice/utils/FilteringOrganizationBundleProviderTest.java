package dk.s4.microservices.organizationalservice.utils;

import ca.uhn.fhir.rest.api.server.IBundleProvider;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FilteringOrganizationBundleProviderTest {


    public static final String blacklistString = "identifier1,identifier2,identifier3";
    private FilteringOrganizationBundleProvider filteringProvider;

    @Before
    public void setup(){
        filteringProvider = new FilteringOrganizationBundleProvider(blacklistString);
    }

    @Test
    public void testFilterEmptyIBundleProvider() {
        IBundleProvider iBundleProvider = mock(IBundleProvider.class);
        when(iBundleProvider.size()).thenReturn(0);

        Bundle returnedBundle = filteringProvider.filter(iBundleProvider);

        assertThat(returnedBundle.getEntry().size()).isEqualTo(0);
    }

    @Test
    public void testFilterIBundleProvider() {
        IBundleProvider iBundleProvider = mock(IBundleProvider.class);
        List<IBaseResource> returnedList = new ArrayList<>();
        returnedList.add(constructOrganization("identifier1"));
        returnedList.add(constructOrganization("somethingElse"));

        when(iBundleProvider.getResources(anyInt(),anyInt())).thenReturn(returnedList);
        when(iBundleProvider.size()).thenReturn(2);

        Bundle returnedBundle = filteringProvider.filter(iBundleProvider);

        assertThat(returnedBundle.getEntry().size()).isEqualTo(1);
        assertThat(((Organization) returnedBundle.getEntry().get(0).getResource()).getIdentifierFirstRep().getSystem()).isEqualTo("system");
        assertThat(((Organization) returnedBundle.getEntry().get(0).getResource()).getIdentifierFirstRep().getValue()).isEqualTo("somethingElse");

    }

    @Test
    public void testFilterIBundleProviderEmptyBlacklist() {
        FilteringOrganizationBundleProvider filteringProvider = new FilteringOrganizationBundleProvider("");
        IBundleProvider iBundleProvider = mock(IBundleProvider.class);
        List<IBaseResource> returnedList = new ArrayList<>();
        returnedList.add(constructOrganization("identifier1"));
        returnedList.add(constructOrganization("somethingElse"));

        when(iBundleProvider.getResources(anyInt(),anyInt())).thenReturn(returnedList);
        when(iBundleProvider.size()).thenReturn(2);

        Bundle returnedBundle = filteringProvider.filter(iBundleProvider);

        assertThat(returnedBundle.getEntry().size()).isEqualTo(2);
        assertThat(((Organization) returnedBundle.getEntry().get(0).getResource()).getIdentifierFirstRep().getSystem()).isEqualTo("system");
        assertThat(((Organization) returnedBundle.getEntry().get(0).getResource()).getIdentifierFirstRep().getValue()).isEqualTo("identifier1");
        assertThat(((Organization) returnedBundle.getEntry().get(1).getResource()).getIdentifierFirstRep().getValue()).isEqualTo("somethingElse");

    }

    private Organization constructOrganization(String identifierValue) {
        Identifier identifier = new Identifier().setValue(identifierValue).setSystem("system");
        Organization organization = new Organization();
        organization.addIdentifier(identifier);
        return organization;
    }

    @Test
    public void testFilterBundle() {
        Bundle bundle = new Bundle();
        bundle.addEntry(createTestEntryComponent("system","identifier1"));
        bundle.addEntry(createTestEntryComponent("system","identifier2"));
        bundle.addEntry(createTestEntryComponent("system","somethingElse"));

        bundle = filteringProvider.filter(bundle);

        assertThat(bundle.getEntry().size()).isEqualTo(1);
        assertThat(((Organization) bundle.getEntry().get(0).getResource()).getIdentifierFirstRep().getSystem()).isEqualTo("system");
        assertThat(((Organization) bundle.getEntry().get(0).getResource()).getIdentifierFirstRep().getValue()).isEqualTo("somethingElse");
    }

    private Bundle.BundleEntryComponent createTestEntryComponent(String system, String identifier) {
        Organization testOrganization = new Organization();
        testOrganization.addIdentifier(new Identifier().setSystem(system).setValue(identifier));
        return new Bundle.BundleEntryComponent().setResource(testOrganization);
    }

    @Test
    public void constructBlackList() {
        Set<String> retVal = filteringProvider.constructBlackList("a,b,c,d,e,f,g,h");

        assertThat(retVal).contains("a");
        assertThat(retVal).contains("b");
        assertThat(retVal).contains("c");
        assertThat(retVal).contains("d");
        assertThat(retVal).contains("e");
        assertThat(retVal).contains("f");
        assertThat(retVal).contains("g");
        assertThat(retVal).contains("h");
        assertThat(retVal).doesNotContain("ab");
        assertThat(retVal).doesNotContain("bc");
        assertThat(retVal).doesNotContain("abcdefgh");
        assertThat(retVal).doesNotContain("invalid");
    }

    @Test
    public void constructEmptyBlackList() {
        Set<String> retVal = filteringProvider.constructBlackList("");

        assertThat(retVal).isEmpty();
    }
}