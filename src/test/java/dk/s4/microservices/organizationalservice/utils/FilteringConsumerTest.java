package dk.s4.microservices.organizationalservice.utils;

import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FilteringConsumerTest {

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private static FilteringConsumer consumer;
    private List<String> identifiers;


    @Before
    public void setup() {
        environmentVariables.set("SOR_BLACKLIST", "identifier_one,identifier_two,identifier_three");
        identifiers = new ArrayList<>();

        IFhirResourceDao<Organization> dao = mock(IFhirResourceDao.class);
        when(dao.create(anyObject())).then( invocationOnMock -> {
            Organization organization = (Organization) invocationOnMock.getArguments()[0];
            identifiers.add(organization.getIdentifierFirstRep().getValue());
            return null;
        });

        consumer = new FilteringConsumer(dao);
    }


    @Test
    public void acceptValidOrganization() {
        Organization organization = new Organization();
        organization.addIdentifier(new Identifier().setValue("identifier_valid"));

        consumer.accept(organization);

        assertThat(identifiers.size()).isEqualTo(1);
        assertThat(identifiers).contains("identifier_valid");
    }


    @Test
    public void isAccepted() {
        Organization organization = new Organization();
        organization.addIdentifier(new Identifier().setValue("identifier_valid"));

        assertThat(consumer.isAccepted(organization)).isTrue();
    }
}